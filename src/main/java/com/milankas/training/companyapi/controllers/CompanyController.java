package com.milankas.training.companyapi.controllers;

import com.milankas.training.companyapi.dto.in.CompanyDto;
import com.milankas.training.companyapi.dto.in.patch.CompanyPatchDto;
import com.milankas.training.companyapi.dto.log.LogStatusDto;
import com.milankas.training.companyapi.dto.out.CompanyOutDto;
import com.milankas.training.companyapi.errors.ErrorResponse;
import com.milankas.training.companyapi.message.MessagingConfig;
import com.milankas.training.companyapi.services.CompanyService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CompanyController {

    private LogStatusDto logStatusDto = new LogStatusDto();

    private CompanyService companyService;
    private RabbitTemplate template;

    public CompanyController(CompanyService companyService, RabbitTemplate template) {
        this.companyService = companyService;
        this.template = template;
    }

    @GetMapping("/v1/companies")
    public List<CompanyOutDto> getAllCompanies(){
        logStatusDto = companyService.buildLogStatusDto("info", "get companies list");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return companyService.getCompanies();
    }

    @GetMapping("/v1/companies/{companyId}")
    public CompanyOutDto getCompany(@PathVariable UUID companyId) {
        if (companyService.getCompany(companyId) == null) {
            logStatusDto = companyService.buildLogStatusDto("error", "get company by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
        else {
            logStatusDto = companyService.buildLogStatusDto("info", "get company by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return companyService.getCompany(companyId);
        }
    }

    @GetMapping("/v1/companies/healthcheck")
    public String healthCheck() {
        logStatusDto = companyService.buildLogStatusDto("info", "get healthcheck");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return "I'm Alive";
    }

    @PostMapping("/v1/companies")
    public ResponseEntity<Object> postCompany(@Valid @RequestBody CompanyDto companyDto, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = companyService.buildLogStatusDto("error", "adding new company");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            logStatusDto = companyService.buildLogStatusDto("info", "adding new company");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return new ResponseEntity<>(companyService.saveCompany(companyDto), HttpStatus.CREATED);
        }
    }

    @DeleteMapping("/v1/companies/{companyId}")
    public ResponseEntity deleteCompany(@PathVariable UUID companyId) {
        if (companyService.deleteCompany(companyId) == null) {
            logStatusDto = companyService.buildLogStatusDto("error", "deleting company");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
        else {
            logStatusDto = companyService.buildLogStatusDto("info", "deleting company");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PatchMapping("/v1/companies/{companyId}")
    public ResponseEntity<Object> patchCompany(@PathVariable UUID companyId,@Valid @RequestBody CompanyPatchDto companyPatchDto, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = companyService.buildLogStatusDto("error", "updating company");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            if (companyService.getCompany(companyId) == null) {
                logStatusDto = companyService.buildLogStatusDto("error", "updating company");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.notFound().build();
            }
            else {
                logStatusDto = companyService.buildLogStatusDto("info", "updating company");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.ok(companyService.updateCompany(companyPatchDto, companyId));
            }
        }
    }


}
