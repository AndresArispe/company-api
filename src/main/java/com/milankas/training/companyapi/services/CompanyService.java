package com.milankas.training.companyapi.services;

import com.milankas.training.companyapi.dto.in.CompanyDto;
import com.milankas.training.companyapi.dto.in.patch.CompanyPatchDto;
import com.milankas.training.companyapi.dto.log.LogStatusDto;
import com.milankas.training.companyapi.dto.out.CompanyOutDto;
import com.milankas.training.companyapi.mappers.in.AddressMapper;
import com.milankas.training.companyapi.mappers.in.CompanyMapper;
import com.milankas.training.companyapi.mappers.in.patch.CompanyPatchMapper;
import com.milankas.training.companyapi.mappers.out.CompanyOutMapper;
import com.milankas.training.companyapi.persistance.model.Address;
import com.milankas.training.companyapi.persistance.model.Company;
import com.milankas.training.companyapi.persistance.repository.AddressRepository;
import com.milankas.training.companyapi.persistance.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CompanyService {

    CompanyRepository companyRepository;
    AddressRepository addressRepository;
    CompanyMapper companyMapper;
    AddressMapper addressMapper;
    CompanyOutMapper companyOutMapper;
    CompanyPatchMapper companyPatchMapper;

    public CompanyService(CompanyPatchMapper companyPatchMapper, CompanyOutMapper companyOutMapper, CompanyRepository companyRepository, AddressRepository addressRepository, CompanyMapper companyMapper, AddressMapper addressMapper) {
        this.companyRepository = companyRepository;
        this.addressRepository = addressRepository;
        this.companyMapper = companyMapper;
        this.addressMapper = addressMapper;
        this.companyOutMapper = companyOutMapper;
        this.companyPatchMapper = companyPatchMapper;
    }

    public List<CompanyOutDto> getCompanies() {
        return companyOutMapper.toCompaniesOutDto(companyRepository.findAll());
    }

    public CompanyOutDto getCompany(UUID companyId) {
        return companyOutMapper.companyToOutDto(companyRepository.findById(companyId).orElse(null));
    }

    public CompanyOutDto saveCompany(CompanyDto companyDto) {
        Company newCompany = companyMapper.toCompany(companyDto);
        Address newAddress = addressMapper.toAddress(companyDto.getAddress());
        newAddress.setCompany(newCompany);
        newCompany.setAddress(newAddress);
        companyRepository.save(newCompany);
        return companyOutMapper.companyToOutDto(newCompany);
    }

    public CompanyOutDto deleteCompany(UUID companyId) {
        Company deletedCompany = companyRepository.findById(companyId).orElse(null);
        if (deletedCompany == null) {
            return null;
        }
        else {
            companyRepository.deleteById(companyId);
            return companyOutMapper.companyToOutDto(deletedCompany);
        }
    }

    public CompanyOutDto updateCompany(CompanyPatchDto companyPatchDto, UUID companyId) {
        Company existingCompany = companyRepository.findById(companyId).orElse(null);
        if (existingCompany == null) {
            return null;
        }
        else {
            companyPatchMapper.updateCompanyFromPatchDto(companyPatchDto, existingCompany);
        }
        return companyOutMapper.companyToOutDto(companyRepository.save(existingCompany));
    }

    public LogStatusDto buildLogStatusDto(String level, String message) {
        LogStatusDto logStatusDto = new LogStatusDto();
        logStatusDto.setMessage(message);
        logStatusDto.setLevel(level);
        logStatusDto.setDateAndTime(new Date());
        logStatusDto.setServiceName("Company-API");
        return logStatusDto;
    }
}
