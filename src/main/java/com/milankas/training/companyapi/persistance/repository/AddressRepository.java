package com.milankas.training.companyapi.persistance.repository;

import com.milankas.training.companyapi.persistance.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AddressRepository extends JpaRepository <Address, UUID> {

}
