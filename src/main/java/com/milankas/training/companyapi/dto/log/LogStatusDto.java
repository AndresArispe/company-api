package com.milankas.training.companyapi.dto.log;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class LogStatusDto {

    private String level;
    private Date dateAndTime;
    private String serviceName;
    private String message;

}
