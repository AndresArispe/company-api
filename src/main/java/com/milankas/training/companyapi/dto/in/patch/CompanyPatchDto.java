package com.milankas.training.companyapi.dto.in.patch;

import com.milankas.training.companyapi.dto.in.AddressDto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class CompanyPatchDto {

    private UUID id;

    @Size(min = 5, max = 12, message = "Name must be have 5 characters minimum and max 12 characters")
    private String name;

    @Valid
    private AddressPatchDto address;
}