package com.milankas.training.companyapi.dto.out;

import com.milankas.training.companyapi.dto.in.AddressDto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class CompanyOutDto {

    private UUID id;

    private String name;
    private AddressDto address;
}
