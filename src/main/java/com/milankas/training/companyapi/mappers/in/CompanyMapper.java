package com.milankas.training.companyapi.mappers.in;

import com.milankas.training.companyapi.dto.in.CompanyDto;
import com.milankas.training.companyapi.dto.in.patch.CompanyPatchDto;
import com.milankas.training.companyapi.persistance.model.Company;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;


import java.util.List;


@Mapper(componentModel = "spring")
public interface CompanyMapper {

    CompanyDto companyToDto(Company company);

    List<CompanyDto> toCompaniesDto(List<Company> companies);

    Company toCompany(CompanyDto companyDto);

}
