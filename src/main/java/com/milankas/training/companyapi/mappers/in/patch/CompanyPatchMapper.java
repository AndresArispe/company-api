package com.milankas.training.companyapi.mappers.in.patch;

import com.milankas.training.companyapi.dto.in.CompanyDto;
import com.milankas.training.companyapi.dto.in.patch.CompanyPatchDto;
import com.milankas.training.companyapi.persistance.model.Company;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CompanyPatchMapper {

    void updateCompanyFromPatchDto(CompanyPatchDto companyPatchDto, @MappingTarget Company entity);

}
