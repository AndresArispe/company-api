package com.milankas.training.companyapi.mappers.in.patch;

import com.milankas.training.companyapi.dto.in.AddressDto;
import com.milankas.training.companyapi.dto.in.patch.AddressPatchDto;
import com.milankas.training.companyapi.persistance.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AddressPatchMapper {

    AddressPatchDto addressToPatchDto(Address address);

    List<AddressPatchDto> toAddressesPatchDto(List<Address> addresses);

    Address toAddress(AddressPatchDto addressPatchDto);
}
