package com.milankas.training.companyapi.mappers.out;

import com.milankas.training.companyapi.dto.in.CompanyDto;
import com.milankas.training.companyapi.dto.out.CompanyOutDto;
import com.milankas.training.companyapi.persistance.model.Company;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CompanyOutMapper {

    CompanyOutDto companyToOutDto(Company company);

    List<CompanyOutDto> toCompaniesOutDto(List<Company> companies);

    Company toCompany(CompanyOutDto companyOutDto);
}